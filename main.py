from pymongo import MongoClient
import ssl
import json
import pandas as pd
import datetime
from flask import jsonify
import numpy as np
from datetime import timedelta
import os

url_mongodb = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)
step_forecast = 0.025


def config_date():
    data = datetime.datetime.utcnow()
    y = data.year
    m = data.month
    d = data.day
    start = datetime.datetime(y, m, d, 0, 0, 0)
    end = datetime.datetime(y, m, d, 23, 59, 59)
    return (start, end)


def get_cursor_forecast(fcst_type, coord_list, days_back):
    start, end = config_date()
    start = start-datetime.timedelta(days=days_back)
    end = end-datetime.timedelta(days=days_back)
    coord_min = np.min(coord_list, axis=0)
    coord_max = np.max(coord_list, axis=0)
    print('min: ', coord_min,
          '\nmax: ', coord_max)
    lon_min, lat_min = coord_min[0], coord_min[1]
    lon_max, lat_max = coord_max[0], coord_max[1]
    client = MongoClient(url_mongodb, ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']
    switcher = {
        "SD10DM": db.SD10DMForecast.find({'coordinates.0': {'$lt': lon_max, '$gte': lon_min}, 'coordinates.1': {'$lt': lat_max, '$gte': lat_min}, 'createdAt': {'$lt': end, '$gte': start}}).sort([['createdAt', -1]]).limit(len(coord_list)),
        "HD5D": db.HD5DForecast.find({'coordinates.0': {'$lt': lon_max, '$gte': lon_min}, 'coordinates.1': {'$lt': lat_max, '$gte': lat_min}, 'createdAt': {'$lt': end, '$gte': start}}).sort([['createdAt', -1]]).limit(len(coord_list)),
        "BRB2": db.BRB2Forecast.find({'coordinates.0': {'$lt': lon_max, '$gte': lon_min}, 'coordinates.1': {'$lt': lat_max, '$gte': lat_min}, 'createdAt': {'$lt': end, '$gte': start}}).sort([['createdAt', -1]]).limit(len(coord_list)),
    }
    return switcher.get(fcst_type, "nothing")


def get_forecast(fcst_type, coord_list):

    df = pd.DataFrame()
    client = MongoClient(url_mongodb, ssl_cert_reqs=ssl.CERT_NONE)
    db = client['api-wtr']

    data = []
    controle = 0
    while(data == [] and (controle <= 4 * step_forecast)):
        coord_data = []
        coord_min = np.array(coord_list) - controle
        coord_max = np.array(coord_list) + controle
        coord_data = [coord_min, coord_max]
        print('>>>>', coord_data)
        cursor = get_cursor_forecast(fcst_type, coord_data, days_back=0)

        for idx, val in enumerate(cursor):
            size = len(val['hourly']['hour'])
            dado = val['hourly']
            df = pd.DataFrame(dado)
            #df['coord'] = [val['coordinates']]*size
            df['lat'] = [val['coordinates'][1]]*size
            df['lon'] = [val['coordinates'][0]]*size
            df = df[['hour', 'lat', 'lon', 'pc', 'rh', 'tp', 'ws']]
            export = df.to_json(orient='table', index=False)
            export = json.loads(export)
            data.append(export['data'])
        if(data == []):
            print('persister')
            cursor = get_cursor_forecast(fcst_type, coord_data, days_back=1)
            for idx, val in enumerate(cursor):
                size = len(val['hourly']['hour'])
                dado = val['hourly']
                df = pd.DataFrame(dado)
                # print(dado)
                #df['coord'] = [val['coordinates']]*size
                df['lat'] = [val['coordinates'][1]]*size
                df['lon'] = [val['coordinates'][0]]*size
                df = df[['hour', 'lat', 'lon', 'pc', 'rh', 'tp', 'ws']]
                export = df.to_json(orient='table', index=False)
                export = json.loads(export)
                data.append(export['data'])
        controle += step_forecast
    print(data)
    return data

# get_forecast('SD10DM', [-46.6591172,-23.557889 ])


def pluvion_chatbot_forecast_get_data(request):
    json_req = request.args.to_dict(flat=False)
    # url/?type_fc=value&lat=value&lon=value
    type_fc = (json_req['type_fc'])[0]
    lat = round(float((json_req['lat'])[0]), 3)
    lon = round(float((json_req['lon'])[0]), 3)
    coord = [lon, lat]

    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    data = get_forecast(type_fc, coord)
    if data == []:
        data = {"status": False}

    return (jsonify(data), 200, headers)


# get_forecast('SD10DM', [-46.659, -23.557])
